//
//  ViewController.m
//  TestBlock
//
//  Created by hw on 16/3/27.
//  Copyright © 2016年 hw. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

typedef long (^BlkSum)(int, int);

@end

@implementation ViewController

- (BlkSum) sumBlock {
    int base = 100;
    BlkSum blk = ^ long (int a, int b) {
        return base + a + b;
    };
    return [blk copy];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view, typically from a nib.
    
    
    BlkSum blk1 = ^ long (int a, int b) {
        return a + b;
    };
    NSLog(@"blk1 = %@", blk1);// blk1 = <__NSGlobalBlock__: 0x47d0>
    
    
    int base = 100;
    BlkSum blk2 = ^ long (int a, int b) {
        return base + a + b;
    };
    NSLog(@"blk2 = %@", blk2); // blk2 = <__NSStackBlock__: 0xbfffddf8>
    
    BlkSum blk3 = [blk2 copy];
    NSLog(@"blk3 = %@", blk3); // blk3 = <__NSMallocBlock__: 0x902fda0>
    
    [self test_success:^(id json) {
        NSLog(@" -->> json:%@", json);
        
    } failure:^(NSError *error) {
        
    }];
}

- (void)test_success:(void (^)(id json))success failure:(void (^)(NSError *error))failure
{
    int a = 10;
    success(@"111");
    NSLog(@" -->> a:%d", a);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
